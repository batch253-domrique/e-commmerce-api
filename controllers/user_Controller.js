const bcrypt = require('bcrypt');
const auth = require('../auth');
const User = require('../models/User_models');
const Product = require('../models/Product_models');
// const Course = require("../models/Course");

// USER REGISTRATION_________________________________________________________________
//redo => "true" for line20 soon...
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
    });

    return newUser
        .save()
        .then((user) => {
            if (user) {
                return true;
            } else {
                return false;
            }
        })
        .catch((err) => err);
};

// USER AUTHENTICATION_________________________________________________________________
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email: reqBody.email })
        .then((result) => {
            if (result == null) {
                return false;
            } else {
                const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
                if (isPasswordCorrect) {
                    return { access: auth.createAccessToken(result) };
                } else {
                    return false;
                }
            }
        })
        .catch((err) => err);
};

// NON-ADMIN USER CHECKOUT (CREATE ORDER)_________________________________________________________________
module.exports.checkout = (data, body) => {
    const id = data.userId;

    return User.findById(id)
        .then((user) => {
            user.orderedProduct.push({ products: body });
            return user.save();
        })
        .catch((error) => {
            console.error(err);
            throw err;
        });
};

//RETRIEVE USER DETAILS_________________________________________________________________
module.exports.getProfile = (data) => {
    return User.findById(data.userId).then((result) => {
        result.password = '';

        return result;
    });
};

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
    return User.find({ email: reqBody.email })
        .then((result) => {
            if (result.length > 0) {
                return true;
            } else {
                return false;
            }
        })
        .catch((err) => err);
};
